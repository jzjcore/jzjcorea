`timescale 1ns/1ps
module top_testing_tb;

reg clock = 1'b0;
wire not_reset = 1'b1;

top_testing testing (.clock(clock), .not_reset(not_reset));

always
begin
    clock = ~clock;
end

initial
begin
    $dumpfile("top_testing.vcd");
    $dumpvars(0,top_testing_tb);
    #1000 $finish;
end

endmodule
