//Partial rewrite of memory module 
module memoryV2
(
	input clock,
	input reset,
	
	output reg [31:0] bus = 32'hz,//we never get data from the bus; we only output data to it
	
	//Status signals for control logic
	output memReadReady,
	output memWriteReady,
	output reg illegalOperation = 1'b0,//read/write not aligned (word to 4 multiple address, half word to 2 multiple address)
	
	//should never both be 1; nothing should be done unless one of these is active
	input memEnable,//we want to do something to memory
	input [1:0] memOperationType,//0 = load instruction, 1 = store instruction, 2 = latch data from currentPC in a temp register, 3 = output data at address of that temp currentPC-derived register
	input [2:0] funct3,//used to deside between lw/lh/lhu/lb/lbu if memOperationType == 0, or between sw/sh/sb if memOperationType == 1
	
	//Values
	input [31:0] portRS1,//rs1
	input [31:0] portRS2,//rs2
	input [31:0] immediateI,//offset for loads
	input [31:0] immediateS,//offset for stores
	
	//Instruction fetching
	input [31:0] currentPC,
	
	//testing
	output [31:0] testOutput
);

//testing
//assign testOutput = reorderedWordOut;

//Parameters, constants and helpers
parameter INITIALIZE_FROM_FILE = 1;
parameter INITIAL_CONTENTS = "rom.mem";
parameter A_WIDTH = 12;

assign memReadReady = 1'b1;//always ready for another read command because reads only take 1 cycle useful for future modules that have delays for some reason (eg. external ram)
assign memWriteReady = 1'b1;//always ready for another write command because writes only take 1 cycle; useful for future modules that have delays for some reason (eg. external ram)

//Wires and registers
reg internalWE;//Should only be enabled if there is not an illegal operation and we want to write
reg latchInCurrentPC;//true if memOperationType = 2
wire [31:0] rawDataOut;//directly from memory
reg [31:0] rawDataIn;//directly to memory
reg [31:0] rawAddress;//raw memory address (risc-v addressing is byte wise but sync_ram is 4 byte wise for faster word reads)
reg [31:0] dataOut;//put onto bus if memOperationType = 0
reg [31:0] tempAddressRegister = 32'h00000000;//holds latched in value of currentPC and is used in memOperationType 3

//Helpers
wire [31:0] loadInstructionAddress = portRS1 + immediateI;//The address to be accessed for load instructions (not a raw physical memory address)
wire [31:0] storeInstructionAddress = portRS1 + immediateS;//The address to be accessed for store instructions (not a raw physical memory address)

/* External Memory Interface */

//Bus output (outputs dataOut to bus if memOperationType = 0 or 3 and illegalOperation = 0 and memEnable = 1; otherwise outputs z)
always @(posedge clock, posedge reset)
begin
	if (reset)
		bus <= 32'hz;
	else if (clock)
	begin
		if (memEnable && ((memOperationType == 2'b00) || (memOperationType == 2'b11)) && !illegalOperation)//we want to and can legally output to the bus
			bus <= dataOut;
		else
			bus <= 32'hz;//ensure bus output is disabled
	end
end

//Memory IO processing (looks at endianness, funct3, memOperationType, and memEnable to determine what to set dataOut, rawDataIn, rawAddress, internalWE, and illegalOperation to)
always @*//todo shrink by puttin some things in a broader scope (eg. latchInCurrentPC can be set to zero in the first case after 3'b000: to save 3 lines of code)
begin
	if (memEnable)
	begin
		case (memOperationType)
			2'b00://load instruction
			begin
				case (funct3)
					3'b000://lb
					begin
						case (loadInstructionAddress % 4)
							0:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highByteOutSE;//sign extended
							end
							1:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highMidByteOutSE;//sign extended
							end
							2:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowMidByteOutSE;//sign extended
							end
							3:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowByteOutSE;//sign extended
							end
							default://what?
							begin
								illegalOperation = 1'b1;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = 32'h00000000;
								dataOut = 32'h00000000;
							end
						endcase
					end
					3'b001://lh
					begin
						if ((loadInstructionAddress % 4) == 0)//address is aligned
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedHighHalfwordOutSE;//sign extended
						end
						else if ((loadInstructionAddress % 4) == 2)//address is aligned
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedLowHalfwordOutSE;//sign extended
						end
						else//Address not aligned, so we don't output anything
						begin
							illegalOperation = 1'b1;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					3'b010://lw
					begin
						if ((loadInstructionAddress % 4) == 0)//address is aligned to a word
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedWordOut;
						end
						else//Address not aligned, so we don't output anything
						begin
							illegalOperation = 1'b1;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					3'b100://lbu
					begin
						case (loadInstructionAddress % 4)
							0:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highByteOutZE;//zero extended
							end
							1:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highMidByteOutZE;//zero extended
							end
							2:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowMidByteOutZE;//zero extended
							end
							3:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowByteOutZE;//zero extended
							end
							default://what?
							begin
								illegalOperation = 1'b1;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = 32'h00000000;
								dataOut = 32'h00000000;
							end
						endcase
					end
					3'b101://lhu
					begin
						if ((loadInstructionAddress % 4) == 0)//address is aligned
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedHighHalfwordOutZE;//zero extended
						end
						else if ((loadInstructionAddress % 4) == 2)//address is aligned
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedLowHalfwordOutZE;//zero extended
						end
						else//Address not aligned, so we don't output anything
						begin
							illegalOperation = 1'b1;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					default://Illegal instruction
					begin
						illegalOperation = 1'b1;
						internalWE = 1'b0;
						latchInCurrentPC = 1'b0;
						rawDataIn  = 32'h00000000;
						rawAddress = 32'h00000000;
						dataOut = 32'h00000000;
					end
				endcase
			end
			2'b01://store instruction
			begin
				case (funct3)
					3'b000://sb
					begin
						case (storeInstructionAddress % 4)
							0:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = highByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							1:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = highMidByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							2:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = lowMidByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							3:
							begin
								illegalOperation = 1'b0;
								internalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = lowByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							default://what?
							begin
								illegalOperation = 1'b1;
								internalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = 32'h00000000;
								dataOut = 32'h00000000;
							end
						endcase
					end
					3'b001://sh
					begin
						if ((storeInstructionAddress % 4) == 0)//address is aligned
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b1;
							latchInCurrentPC = 1'b0;
							rawDataIn  = reorderedHighHalfwordIn;
							rawAddress = storeInstructionAddress / 4;
							dataOut = 32'h00000000;
						end
						else if ((loadInstructionAddress % 4) == 2)//address is aligned
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b1;
							latchInCurrentPC = 1'b0;
							rawDataIn  = reorderedLowHalfwordIn;
							rawAddress = storeInstructionAddress / 4;
							dataOut = 32'h00000000;
						end
						else//Address not aligned, so we don't write anything
						begin
							illegalOperation = 1'b1;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					3'b010://sw
					begin
						if ((storeInstructionAddress % 4) == 0)//address is aligned to a word
						begin
							illegalOperation = 1'b0;
							internalWE = 1'b1;
							latchInCurrentPC = 1'b0;
							rawDataIn  = reorderedWordIn;
							rawAddress = storeInstructionAddress / 4;
							dataOut = 32'h00000000;
						end
						else//Address not aligned, so we don't write anything
						begin
							illegalOperation = 1'b1;
							internalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					default://Illegal instruction
					begin
						illegalOperation = 1'b1;
						internalWE = 1'b0;
						latchInCurrentPC = 1'b0;
						rawDataIn  = 32'h00000000;
						rawAddress = 32'h00000000;
						dataOut = 32'h00000000;
					end
				endcase
			end
			2'b10://latch in currentPC
			begin
				illegalOperation = 1'b0;
				internalWE = 1'b0;
				latchInCurrentPC = 1'b1;
				rawDataIn  = 32'h00000000;
				rawAddress = 32'h00000000;
				dataOut = 32'h00000000;
			end
			2'b11://output word at address of tempAddressRegister (previously set by case 2'b10)
			begin
				if ((tempAddressRegister % 4) == 0)//address is aligned to a word
				begin
					illegalOperation = 1'b0;
					internalWE = 1'b0;
					latchInCurrentPC = 1'b0;
					rawDataIn  = 32'h00000000;
					rawAddress = tempAddressRegister / 4;
					dataOut = reorderedWordOut;
				end
				else//Address not aligned, so we don't output anything
				begin
					illegalOperation = 1'b1;
					internalWE = 1'b0;
					latchInCurrentPC = 1'b0;
					rawDataIn  = 32'h00000000;
					rawAddress = 32'h00000000;
					dataOut = 32'h00000000;
				end
			end
			default://Should not ever happen
			begin
				illegalOperation = 1'b1;
				internalWE = 1'b0;
				latchInCurrentPC = 1'b0;
				rawDataIn  = 32'h00000000;
				rawAddress = 32'h00000000;
				dataOut = 32'h00000000;
			end
		endcase
	end
	else
	begin//we do nothing because memEnable is not enabled
		illegalOperation = 1'b0;
		internalWE = 1'b0;
		latchInCurrentPC = 1'b0;
		rawDataIn  = 32'h00000000;
		rawAddress = 32'h00000000;
		dataOut = 32'h00000000;
	end
end

//currentPC latching into tempAddressRegister if memOperationType = 2 and memEnable = 1
always @(posedge clock, posedge reset)
begin
	if (reset)
		tempAddressRegister <= 32'h00000000;
	else if (clock)
	begin
		if (memEnable && latchInCurrentPC)//mem is enabled and we want to latch in pc
			tempAddressRegister <= currentPC;
	end
end

/* Raw memory storage and endianness/io translation/helpers */
//Sign extention and zero extention of reordered/extracted data output
wire [31:0] reorderedHighHalfwordOutSE = {{16{reorderedHighHalfwordOut[15]}}, reorderedHighHalfwordOut};//lh
wire [31:0] reorderedHighHalfwordOutZE = {16'h0000, reorderedHighHalfwordOut};//lhu
wire [31:0] reorderedLowHalfwordOutSE = {{16{reorderedLowHalfwordOut[15]}}, reorderedLowHalfwordOut};//lh
wire [31:0] reorderedLowHalfwordOutZE = {16'h0000, reorderedLowHalfwordOut};//lhu
wire [31:0] highByteOutSE = {{24{highByteOut[7]}}, highByteOut};//lb
wire [31:0] highByteOutZE = {24'h000000, highByteOut};//lbu
wire [31:0] highMidByteOutSE = {{24{highMidByteOut[7]}}, highMidByteOut};//lb
wire [31:0] highMidByteOutZE = {24'h000000, highMidByteOut};//lbu
wire [31:0] lowMidByteOutSE = {{24{lowMidByteOut[7]}}, lowMidByteOut};//lb
wire [31:0] lowMidByteOutZE = {24'h000000, lowMidByteOut};//lbu
wire [31:0] lowByteOutSE = {{24{lowByteOut[7]}}, lowByteOut};//lb
wire [31:0] lowByteOutZE = {24'h000000, lowByteOut};//lbu
//Conversion of rawDataOut to a properly ordered format for memory reads
wire [31:0] reorderedWordOut = {rawDataOut[7:0], rawDataOut[15:8], rawDataOut[23:16], rawDataOut[31:24]};//internal address offset must be 0; lw
wire [15:0] reorderedHighHalfwordOut = {rawDataOut[23:16], rawDataOut[31:24]};//internal address offset must be 0
wire [15:0] reorderedLowHalfwordOut = {rawDataOut[7:0], rawDataOut[15:8]};//internal address offset must be 2
wire [7:0] highByteOut = {rawDataOut[31:24]};//internal address offset must be 0
wire [7:0] highMidByteOut = {rawDataOut[23:16]};//internal address offset must be 1
wire [7:0] lowMidByteOut = {rawDataOut[15:8]};//internal address offset must be 2
wire [7:0] lowByteOut = {rawDataOut[7:0]};//internal address offset must be 3
//Reordering of portRS2 (which contains data to be writeen) to little endian format; also preserves original data where needed (use these by setting rawDataIn to one of these)
wire [31:0] reorderedWordIn = {portRS2[7:0], portRS2[15:8], portRS2[23:16], portRS2[31:24]};//internalAddressOffset must be 0; sw
wire [31:0] reorderedHighHalfwordIn = {portRS2[7:0], portRS2[15:8], rawDataOut[15:0]};//internalAddressOffset must be 0; preserves bottom 2 bytes; sh
wire [31:0] reorderedLowHalfwordIn = {rawDataOut[31:16], portRS2[7:0], portRS2[15:8]};//internalAddressOffset must be 2; preserves upper 2 bytes; sh
wire [31:0] highByteIn = {portRS2[31:24], rawDataOut[23:0]};//internalAddressOffset must be 0; preserves lower 3 bytes; sb
wire [31:0] highMidByteIn = {rawDataOut[31:24], portRS2[23:16], rawDataOut[15:0]};//internalAddressOffset must be 1; preserves upper 1 and lower 2 bytes; sb
wire [31:0] lowMidByteIn = {rawDataOut[31:16], portRS2[15:8], rawDataOut[7:0]};//internalAddressOffset must be 2; preserves upper 2 and lower 1 bytes; sb
wire [31:0] lowByteIn = {rawDataOut[31:8], portRS2[7:0]};//internalAddressOffset must be 3; preserves upper 3 bytes; sb

//Raw memory module (todo make a wrapper module for this one that catches addresses to be used for memory mapped io)
sync_ram #(.FILE(INITIAL_CONTENTS), .A_WIDTH(A_WIDTH), .D_WIDTH(32), .INITIALIZE_FROM_FILE(INITIALIZE_FROM_FILE)) (.clock(clock), .write_enable(internalWE),
			  .data_out(rawDataOut), .read_address(rawAddress), .data_in(rawDataIn), .write_address(rawAddress));
endmodule