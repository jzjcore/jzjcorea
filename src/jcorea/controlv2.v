module controlV2//New version of control. Slower, but works
(
	input clock,
	input reset,
	
	//inout reg [31:0] bus = 32'hz,//we acctually don't need a bus for any part of the instruction cycle or instructions (at least not so far)
	
	//Memory feedback
	input memReadReady,//ready for another read
	input memWriteReady,//ready for another write
	input instructionReady,//ready for another instruction fetch
	
	//Control lines (tell other modules to interpret various parts of the instruction themselves)
	output reg registerFileWE,//the data on the bus should be written to the value in rd
	output reg aluOE,//Signals the type of opcode is for the alu so it can interpret the rest of the instruction itself (from funct7[5] and funct3)
	output reg aluRR,//Signals the alu opcode is a register register operation, not a register immediate operation
	output reg memEnable,//Signals a memory command (specified in memOperationType)
	output reg [1:0] memOperationType,//0 is a load instruction, 1 is a store instruction, 2 latches currentPC in a temp register, 3 reads data from the address contained in that temp register
	output reg pcWE,//set pc to contents on bus
	output reg pcInc,//increment pc by 4 next clock cycle
	output reg luiauipcEnable,//do a lui or an auipc instruction
	output reg luiauipcOperation,//choose between lui (0) or auipc (1)
	output reg enableJumpLogic,//enable control flow transfer logic
	output reg [1:0] jumpLogicOperationType,//0 = jal, 1 = jalr, 2 = branch instructions, 3 = latch in currentPC
	output reg outputNewPCJumpLogic,//output new pc (1) or new rd value (0) to bus
	
	//Instruction fetch directly from memory module
	input [31:0] instructionIn,
	
	//External export of instruction
	//Note no matter what this changes to modules cannot do anything without being told by the above control signals
	//They can do things internaly based on the below stuffsbut it must not affect their outward facing state or the
	//outward facing state of other modules
	output [6:0] funct7,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	//output [6:0] opcode,//the control logic is suppost to handle this, not other modules, so we don't expose it
	//Immediates
	output [31:0] immediateI,
	output [31:0] immediateS,
	output [31:0] immediateB,
	output [31:0] immediateU,
	output [31:0] immediateJ,
	
	//Exception lines
	input pcMisaligned,//pc is not aligned to a 4 byte address
	input illegalMemOperation//unaligned memory access or illegal funct3
);
//Instruction stuff
wire [6:0] opcode;
reg [31:0] instructionRegister = 32'h00000000;

//States
localparam STATE_PCINC_AND_MARSET = 9'b000000001;//Sets pc to increment by 4 and latches old value of pc into mar//"Fetch"
localparam STATE_OUTPUT_INSTRUCTION = 9'b000000010;//"Decode"
localparam STATE_LATCH_INSTRUCTION = 9'b000000100;
localparam STATE_EXECUTEA = 9'b000001000;//Not all execute steps will need to be used for most instructions//"Execute"
localparam STATE_EXECUTEB = 9'b000010000;
localparam STATE_EXECUTEC = 9'b000100000;
localparam STATE_EXECUTED = 9'b001000000;
localparam STATE_EXECUTEE = 9'b010000000;
localparam STATE_HALT = 9'b100000000;

reg [8:0] currentState = STATE_PCINC_AND_MARSET;
reg [8:0] nextState;
reg [3:0] statesNeeded;//shows how many states are needed by the instruction; 0 means only EXECUTEA, 1 means EXECUTEA and B, and so on
reg halt = 1'b0;//ecall/ebreak or something illegal or an exception

//State management
always @(negedge clock, posedge reset)
begin
	if (reset)
		currentState <= STATE_PCINC_AND_MARSET;
	else if (~clock)//we're trigering on the negative edge
		currentState <= nextState;//current_state becomes new_state each clock pulse
	else//This shouldn't be needed, but I am terrified of inferred latches so this is the anti-inferred-latches-warning superstisious object thing//todo figure out why this is needed
		currentState <= STATE_PCINC_AND_MARSET;
end

//State transfer logic
always @*
begin
	if (halt || pcMisaligned || illegalMemOperation)//reasons to halt
		nextState = STATE_HALT;//halting the cpu
	else//we're not halting the cpu
	begin
		case (currentState)
			STATE_PCINC_AND_MARSET:
			begin
				nextState = STATE_OUTPUT_INSTRUCTION;//Should only ever take 1 clock so we just move to the next state immediatly
			end
			STATE_OUTPUT_INSTRUCTION:
			begin
				if (instructionReady)
					nextState = STATE_LATCH_INSTRUCTION;//we will have read the instruction into the instructionRegister already as soon as memReadReady shows the memory has finished fetching it, so we can move to the next state
				else
					nextState = STATE_OUTPUT_INSTRUCTION;
			end
			STATE_LATCH_INSTRUCTION:
			begin
				nextState = STATE_EXECUTEA;
			end
			STATE_EXECUTEA:
			begin
				if (statesNeeded == 0)
					nextState = STATE_PCINC_AND_MARSET;
				else
					nextState = STATE_EXECUTEB;
			end
			STATE_EXECUTEB:
			begin
				if (statesNeeded <= 1)
					nextState = STATE_PCINC_AND_MARSET;
				else
					nextState = STATE_EXECUTEC;
			end
			STATE_EXECUTEC:
			begin
				if (statesNeeded <= 2)
					nextState = STATE_PCINC_AND_MARSET;
				else
					nextState = STATE_EXECUTED;
			end
			STATE_EXECUTED:
			begin//we only have 4 execute states so far
				if (statesNeeded <= 3)
					nextState = STATE_PCINC_AND_MARSET;
				else
					nextState = STATE_EXECUTEE;
			end
			STATE_EXECUTEE:
			begin//we only have 5 execute states so far
				//if (statesNeeded <= 4)
					nextState = STATE_PCINC_AND_MARSET;
				//else
				//	nextState = STATE_EXECUTEF;
			end
			STATE_HALT://only happens if halt is somehow 0 but currentState == STATE_HALT
			begin
				nextState = STATE_HALT;//halting the cpu
			end
			default: nextState = STATE_PCINC_AND_MARSET;
		endcase
	end
end

//State operations
always @(negedge clock, posedge reset)
begin
	if (reset)//Reset all of the control lines
	begin
		instructionRegister <= 32'h00000000;
		registerFileWE <= 1'b0;
		aluOE <= 1'b0;
		aluRR <= 1'b0;
		memEnable <= 1'b0;
		memOperationType <= 2'b00;
		pcWE <= 1'b0;
		pcInc <= 1'b0;
		luiauipcEnable <= 1'b0;
		luiauipcOperation <= 1'b0;
		enableJumpLogic <= 1'b0;
		jumpLogicOperationType <= 2'b00;
		outputNewPCJumpLogic <= 1'b0;
		halt <= 1'b0;
	end
	else if (~clock)//we're trigering on the negative edge
	begin
		case (currentState)
			STATE_PCINC_AND_MARSET:
			begin
				//Reset whatever the instructions were doing
				registerFileWE <= 1'b0;
				aluOE <= 1'b0;
				aluRR <= 1'b0;
				pcWE <= 1'b0;
				luiauipcEnable <= 1'b0;
				luiauipcOperation <= 1'b0;
				enableJumpLogic <= 1'b0;
				jumpLogicOperationType <= 2'b00;
				outputNewPCJumpLogic <= 1'b0;
				halt <= 1'b0;
			
				memEnable <= 1'b1;
				memOperationType <= 2'b10;//we copy the old data in the pc into the instruction mar...
				pcInc <= 1'b1;//at the same time we increment the pc
			end
			STATE_OUTPUT_INSTRUCTION:
			begin
				//Reset things from last state
				pcInc <= 1'b0;
				
				memEnable <= 1'b1;//give us our juicy instruction (yum!)
				memOperationType <= 2'b11;//will output instruction to instructionIn at the next posedge
			end
			STATE_LATCH_INSTRUCTION:
			begin
				//Keep outputting instruction//not needed because instruction is held in a temporary register in the memory module until the next posedge
				//memEnable <= 1'b1;//probably don't need to keep this active
				//memOperationType <= 2'b11;//probably don't need to keep this active
				memEnable <= 1'b0;
				memOperationType <= 2'b00;
				
				instructionRegister <= instructionIn;//Yes, I'll take 1 instruction please!
			end
			STATE_EXECUTEA:
			begin
				//Reset things from last state
				//nothing is actually still set from last state
				
				case (opcode)//Now that we have the instruction, we can look at the opcode and tell modules to do things on the next clock!
					7'b0010011://Alu immediate operation instructions
					begin
						aluOE <= 1'b1;//Output result of alu (uses the i immediate and rs1 by default)
					end
					7'b0110011://Alu register-register operation instructions
					begin
						aluRR <= 1'b1;//Alu register-register operation
						aluOE <= 1'b1;//Output result of alu
					end
					7'b0000011://memory read instructions
					begin
						memEnable <= 1'b1;//enable memory
						memOperationType <= 2'b00;//output data to bus
						
						//for better flexibility with different memory modules, we should use the memReadReady flag, but we know that memoryv4 takes 2 clock cycles for a read
						//so we'll just hard code this for now
						//if (!memReadReady)
						//	stateWait <= 1'b1;
						//else
						//	stateWait <= 1'b0;
					end
					7'b0100011://memory write instructions
					begin
						memEnable <= 1'b1;//enable memory
						memOperationType <= 2'b01;//write data to memory
					end
					7'b0110111://lui
					begin
						luiauipcEnable <= 1;
						luiauipcOperation <= 0;//lui
					end
					7'b0010111://auipc
					begin
						luiauipcEnable <= 1;
						luiauipcOperation <= 1;//auipc
					end
					7'b0001111://fence/fence.i
					begin
						//nothing
					end
					7'b1110011://ecall/ebreak
					begin
						halt <= 1;//ecalls/ebreaks just cause a fatal trap
					end
					7'b1101111://jal
					begin
						enableJumpLogic <= 1'b1;//we're jumpin'
						jumpLogicOperationType <= 2'b11;//first we latch in the current value of currentPC
					end
					7'b1100111://jalr
					begin
						enableJumpLogic <= 1'b1;//we're jumpin'
						jumpLogicOperationType <= 2'b11;//first we latch in the current value of currentPC
					end
					7'b1100011://branch instructions
					begin
						enableJumpLogic <= 1'b1;//we're jumpin'
						jumpLogicOperationType <= 2'b11;//first we latch in the current value of currentPC
					end
					default://something went wrong
					begin
						halt <= 1'b1;
					end
				endcase
			end
			STATE_EXECUTEB:
			begin
				case (opcode)//Now that we have the instruction, we can look at the opcode and tell modules to do things on the next clock!
					7'b0010011://Alu immediate operation instructions
					begin
						registerFileWE <= 1'b1;//Save our result into rd
					end
					7'b0110011://Alu register-register operation instructions
					begin
						registerFileWE <= 1'b1;//Save our result into rd
					end
					7'b0000011://memory read instructions
					begin
						//keep outputting to bus; reads take 2 cycles; still waiting on memory
						memEnable <= 1'b1;//enable memory
						memOperationType <= 2'b00;//output data to bus
					end
					7'b0100011://memory write instructions (sb and sh only for read+modify then write cycle)
					begin
						memEnable <= 1'b1;//continue enabling memory
						memOperationType <= 2'b01;//continue writing data to memory
					end
					7'b0110111://lui
					begin
						registerFileWE <= 1'b1;//save lui data into rd
					end
					7'b0010111://auipc
					begin
						registerFileWE <= 1'b1;//save auipc data into rd
					end
					7'b1101111://jal
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b00;//the instruction is jal
						outputNewPCJumpLogic <= 1'b1;//output new pc
					end
					7'b1100111://jalr
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b01;//the instruction is jalr
						outputNewPCJumpLogic <= 1'b1;//output new pc
					end
					7'b1100011://branch instructions
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b10;//a branch instruction
						outputNewPCJumpLogic <= 1'b1;//output new pc
					end
					default://something went wrong
					begin
						halt <= 1'b1;
					end
				endcase
			end
			STATE_EXECUTEC:
			begin
				case (opcode)//Now that we have the instruction, we can look at the opcode and tell modules to do things on the next clock!
					7'b0000011://memory read instructions
					begin
						//keep outputting to bus//Data goes onto bus on next posedge, so the posedge after that (STATE_EXECUTED) we read in the data
						//memEnable <= 1'b1;//enable memory
						//memOperationType <= 2'b00;//output data to bus
						
						registerFileWE <= 1'b1;//finally
					end
					7'b1101111://jal
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b00;//the instruction is jal
						outputNewPCJumpLogic <= 1'b0;//output new rd
						pcWE <= 1'b1;//latch new pc
					end
					7'b1100111://jalr
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b01;//the instruction is jalr
						outputNewPCJumpLogic <= 1'b0;//output new rd
						pcWE <= 1'b1;//latch new pc
					end
					7'b1100011://branch instructions
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b10;//a branch instruction
						pcWE <= 1'b1;//latch new pc
					end
					default://something went wrong
					begin
						halt <= 1'b1;
					end
				endcase
			end
			STATE_EXECUTED:
			begin
				case (opcode)//Now that we have the instruction, we can look at the opcode and tell modules to do things on the next clock!
					7'b1101111://jal
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b00;//the instruction is jal
						outputNewPCJumpLogic <= 1'b0;//continue outputing new rd 
						pcWE <= 1'b0;//stop latching pc
						registerFileWE <= 1'b1;//latch new rd
					end
					7'b1100111://jalr
					begin
						enableJumpLogic <= 1'b1;//still jumpin'
						jumpLogicOperationType <= 2'b01;//the instruction is jalr
						outputNewPCJumpLogic <= 1'b0;//continue outputing new rd 
						pcWE <= 1'b0;//stop latching pc
						registerFileWE <= 1'b1;//latch new rd
					end
					default://something went wrong
					begin
						halt <= 1'b1;
					end
				endcase
			end
			STATE_EXECUTEE:
			begin
				case (opcode)//Now that we have the instruction, we can look at the opcode and tell modules to do things on the next clock!
					default://something went wrong
					begin
						halt <= 1'b1;
					end
				endcase
			end
			STATE_HALT://au revoir
			begin
				instructionRegister <= 32'h00000000;
				registerFileWE <= 1'b0;
				aluOE <= 1'b0;
				aluRR <= 1'b0;
				memEnable <= 1'b0;
				memOperationType <= 2'b00;
				pcWE <= 1'b0;
				pcInc <= 1'b0;
				luiauipcEnable <= 0;
				luiauipcOperation <= 0;
				enableJumpLogic <= 1'b0;
				jumpLogicOperationType <= 2'b00;
				outputNewPCJumpLogic <= 1'b0;
				halt <= 1'b1;
			end
		endcase
	end
end

//Number of execution states needed for different instructions
//0 is 1 state (a), 1 is 2 states (a and b), and so on
always @*
begin
	case (opcode)
		7'b0010011: statesNeeded = 1;//alu immediate operation
		7'b0110011: statesNeeded = 1;//alu register-register operation (from base spec or m extension)
		7'b0000011: statesNeeded = 2;//memory read
		7'b0100011://memory write
		begin
			if (funct3 == 3'b010)
				statesNeeded = 0;//sw
			else
				statesNeeded = 1;//sb or sh because of read+modify-write cycle
		end
		7'b0110111: statesNeeded = 1;//lui
		7'b0010111: statesNeeded = 1;//auipc
		7'b0001111: statesNeeded = 0;//fence/fence.i
		7'b1110011: statesNeeded = 0;//ecall/ebreak
		7'b1101111: statesNeeded = 3;//jal
		7'b1100111: statesNeeded = 3;//jalr
		7'b1100011: statesNeeded = 2;//branch instructions
		default statesNeeded = 0;//if we have a bad opcode, we halt which only take 1 execute cycle
	endcase
end

//Instruction interpretation
instructionInterpretV2 iiv2 (.instruction(instructionRegister), .funct7(funct7), .rs2(rs2), .rs1(rs1), .funct3(funct3), .rd(rd), .opcode(opcode),
									  .immediateI(immediateI), .immediateS(immediateS), . immediateB(immediateB), .immediateU(immediateU), .immediateJ(immediateJ));

endmodule
