module memoryV6//Final final final version of memory module for jzjcore revision a (changes from sync_ram to an indirect memoryBackend backend, allowing for memory mapped io and 4 ports!)
(
	/* Note: a memory read takes 2 cycles; one to fetch from the fpga ram, and another to process it/output to bus
		Whole word writes and instruction fetches skip the bus, so they only take 1 cycle to execute
		Byte or half word writes take 2 clock cycles because of the physically 32bit memory layout which needs a read+modify-write cycle
	*/

	input clock,
	input reset,
	
	output reg [31:0] bus = 32'hz,//we never get data from the bus; we only output data to it
	
	//Status signals for control logic
	output reg memReadReady = 1'b1,
	output reg memWriteReady,//todo ensure this is being set correctly
	output reg illegalOperation = 1'b0,//read/write not aligned (word to 4 multiple address, half word to 2 multiple address)
	
	input memEnable,//we want to do something to memory
	input [1:0] memOperationType,//0 = load instruction, 1 = store instruction, 2 = latch data from currentPC in a temp register, 3 = output data at address of that temp currentPC-derived register
	input [2:0] funct3,//used to deside between lw/lh/lhu/lb/lbu if memOperationType == 0, or between sw/sh/sb if memOperationType == 1
	
	//Values
	input [31:0] portRS1,//rs1
	input [31:0] portRS2,//rs2
	input [31:0] immediateI,//offset for loads
	input [31:0] immediateS,//offset for stores
	
	//Instruction fetching
	input [31:0] currentPC,
	output [31:0] instructionOut,//always equal to dataOut which saves having to shift dataOut onto the bus, saving a clock cycle
	output instructionReady,//instruction is ready to be read from instructionOut
	
	//CPU memory mapped io ports (for io dir, 0 = read, 1 = write)
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portX[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portX[0]
	//and for direction setting rs2[0] = portXDirMemAddress[24] = portXDirOfPin[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Addresses for port read/write   |   io direction (0 for input, 1 for output)
	inout [31:0] mmPortA,//FFFFFFF0   |   FFFFFFE0
	inout [31:0] mmPortB,//FFFFFFF4   |   FFFFFFE4
	inout [31:0] mmPortC,//FFFFFFF8   |   FFFFFFE8
	inout [31:0] mmPortD,//FFFFFFFC   |   FFFFFFEC
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems when compiling
	//portX[Y] can only be written to by an external moduleif it is not in output mode
	//if portXDir[y] is 1 then that pin is outputting data, if portXDir[y] is 0 then that pin is high impedance
	output [31:0] mmPortADir,
	output [31:0] mmPortBDir,
	output [31:0] mmPortCDir,
	output [31:0] mmPortDDir
);
//Parameters, constants and helpers
parameter INITIALIZE_FROM_FILE = 1;
parameter INITIAL_CONTENTS = "rom.mem";
parameter A_WIDTH = 12;

assign instructionReady = 1'b1;//always ready for another instruction fetch because directly accessing dataOut only takes 1 cycle; useful for future modules that have delays for some reason (eg. external ram)
assign instructionOut = dataOut;//we save a clock cycle by accessing instruction out directly

//Wires and registers
reg wantsInternalWE;//held high by memory io processing logic which indicates to internalWE logic to wait for rawDataOut on the first clock cycle and then enable internalWE on the second one
reg internalWE = 1'b0;//Should only be enabled if there is not an illegal operation and we want to write
reg latchInCurrentPC;//true if memOperationType = 2
wire [31:0] rawDataOut;//directly from memory
reg [31:0] rawDataIn;//directly to memory
reg [31:0] rawAddress;//raw memory address (risc-v addressing is byte wise but sync_ram is 4 byte wise for faster word reads)
reg [31:0] dataOut;//put onto bus if memOperationType = 0
reg [31:0] tempAddressRegister = 32'h00000000;//holds latched in value of currentPC and is used in memOperationType 3
reg readWait = 1'b0;//0 for one cycle, then 1 when the read is ready

//Helpers
wire [31:0] loadInstructionAddress = portRS1 + immediateI;//The address to be accessed for load instructions (not a raw physical memory address)
wire [31:0] storeInstructionAddress = portRS1 + immediateS;//The address to be accessed for store instructions (not a raw physical memory address)

/* External Memory Interface */

//Bus output (outputs dataOut to bus if memOperationType = 0 or 3 and illegalOperation = 0 and memEnable = 1; otherwise outputs z)
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		bus <= 32'hz;
		memReadReady <= 1'b1;
		readWait <= 1'b0;
	end
	else if (clock)
	begin
		if (memEnable && (memOperationType == 2'b00) && !illegalOperation)//we want to and can legally output to the bus
		begin
			//Reads only execute once every 2 clock cycles//fixme even though memReadReady is false, garbage data is still output to the bus on clock 1 and then proper data on clock 2. Fix so data is only output to bus on clock 2.
			if (readWait == 1'b0)
			begin
				readWait <= 1;
				bus <= 32'hz;//not working for some reason
				memReadReady <= 0;
			end
			else// if (readWait == 1'b1)
			begin
				bus <= dataOut;
				readWait <= 0;
				memReadReady <= 1;
			end
		end
		else
		begin
			bus <= 32'hz;//ensure bus output is disabled
			readWait <= 1'b0;//reset readWait counter
			memReadReady <= 1'b0;//the data is not ready to be read because a read command was not sent
		end
	end
end

//Memory IO processing (looks at endianness, funct3, memOperationType, and memEnable to determine what to set dataOut, rawDataIn, rawAddress, wantsInternalWE, and illegalOperation to)
always @*//todo shrink by putting some things in a broader scope (eg. latchInCurrentPC can be set to zero in the first case after 3'b000: to save 3 lines of code)
begin
	if (memEnable)
	begin
		case (memOperationType)
			2'b00://load instruction
			begin
				case (funct3)
					3'b000://lb
					begin
						case (loadInstructionAddress % 4)
							0:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highByteOutSE;//sign extended
							end
							1:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highMidByteOutSE;//sign extended
							end
							2:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowMidByteOutSE;//sign extended
							end
							3:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowByteOutSE;//sign extended
							end
							default://what?
							begin
								illegalOperation = 1'b1;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = 32'h00000000;
								dataOut = 32'h00000000;
							end
						endcase
					end
					3'b001://lh
					begin
						if ((loadInstructionAddress % 4) == 0)//address is aligned
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedHighHalfwordOutSE;//sign extended
						end
						else if ((loadInstructionAddress % 4) == 2)//address is aligned
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedLowHalfwordOutSE;//sign extended
						end
						else//Address not aligned, so we don't output anything
						begin
							illegalOperation = 1'b1;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					3'b010://lw
					begin
						if ((loadInstructionAddress % 4) == 0)//address is aligned to a word
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedWordOut;
						end
						else//Address not aligned, so we don't output anything
						begin
							illegalOperation = 1'b1;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					3'b100://lbu
					begin
						case (loadInstructionAddress % 4)
							0:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highByteOutZE;//zero extended
							end
							1:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = highMidByteOutZE;//zero extended
							end
							2:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowMidByteOutZE;//zero extended
							end
							3:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = loadInstructionAddress / 4;
								dataOut = lowByteOutZE;//zero extended
							end
							default://what?
							begin
								illegalOperation = 1'b1;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = 32'h00000000;
								dataOut = 32'h00000000;
							end
						endcase
					end
					3'b101://lhu
					begin
						if ((loadInstructionAddress % 4) == 0)//address is aligned
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedHighHalfwordOutZE;//zero extended
						end
						else if ((loadInstructionAddress % 4) == 2)//address is aligned
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = loadInstructionAddress / 4;
							dataOut = reorderedLowHalfwordOutZE;//zero extended
						end
						else//Address not aligned, so we don't output anything
						begin
							illegalOperation = 1'b1;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					default://Illegal instruction
					begin
						illegalOperation = 1'b1;
						wantsInternalWE = 1'b0;
						latchInCurrentPC = 1'b0;
						rawDataIn  = 32'h00000000;
						rawAddress = 32'h00000000;
						dataOut = 32'h00000000;
					end
				endcase
			end
			2'b01://store instruction
			begin
				case (funct3)
					3'b000://sb
					begin
						case (storeInstructionAddress % 4)
							0:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = highByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							1:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = highMidByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							2:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = lowMidByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							3:
							begin
								illegalOperation = 1'b0;
								wantsInternalWE = 1'b1;
								latchInCurrentPC = 1'b0;
								rawDataIn  = lowByteIn;
								rawAddress = storeInstructionAddress / 4;
								dataOut = 32'h00000000;
							end
							default://what?
							begin
								illegalOperation = 1'b1;
								wantsInternalWE = 1'b0;
								latchInCurrentPC = 1'b0;
								rawDataIn  = 32'h00000000;
								rawAddress = 32'h00000000;
								dataOut = 32'h00000000;
							end
						endcase
					end
					3'b001://sh
					begin
						if ((storeInstructionAddress % 4) == 0)//address is aligned
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b1;
							latchInCurrentPC = 1'b0;
							rawDataIn  = reorderedHighHalfwordIn;
							rawAddress = storeInstructionAddress / 4;
							dataOut = 32'h00000000;
						end
						else if ((loadInstructionAddress % 4) == 2)//address is aligned
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b1;
							latchInCurrentPC = 1'b0;
							rawDataIn  = reorderedLowHalfwordIn;
							rawAddress = storeInstructionAddress / 4;
							dataOut = 32'h00000000;
						end
						else//Address not aligned, so we don't write anything
						begin
							illegalOperation = 1'b1;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					3'b010://sw
					begin
						if ((storeInstructionAddress % 4) == 0)//address is aligned to a word
						begin
							illegalOperation = 1'b0;
							wantsInternalWE = 1'b1;
							latchInCurrentPC = 1'b0;
							rawDataIn  = reorderedWordIn;
							rawAddress = storeInstructionAddress / 4;
							dataOut = 32'h00000000;
						end
						else//Address not aligned, so we don't write anything
						begin
							illegalOperation = 1'b1;
							wantsInternalWE = 1'b0;
							latchInCurrentPC = 1'b0;
							rawDataIn  = 32'h00000000;
							rawAddress = 32'h00000000;
							dataOut = 32'h00000000;
						end
					end
					default://Illegal instruction
					begin
						illegalOperation = 1'b1;
						wantsInternalWE = 1'b0;
						latchInCurrentPC = 1'b0;
						rawDataIn  = 32'h00000000;
						rawAddress = 32'h00000000;
						dataOut = 32'h00000000;
					end
				endcase
			end
			2'b10://latch in currentPC
			begin
				illegalOperation = 1'b0;
				wantsInternalWE = 1'b0;
				latchInCurrentPC = 1'b1;
				rawDataIn  = 32'h00000000;
				rawAddress = 32'h00000000;
				dataOut = 32'h00000000;
			end
			2'b11://output word at address of tempAddressRegister to instructionOut (address previously set by case 2'b10)
			begin
				if ((tempAddressRegister % 4) == 0)//address is aligned to a word
				begin
					illegalOperation = 1'b0;
					wantsInternalWE = 1'b0;
					latchInCurrentPC = 1'b0;
					rawDataIn  = 32'h00000000;
					rawAddress = tempAddressRegister / 4;
					dataOut = reorderedWordOut;//instructionOut is assigned to dataOut
				end
				else//Address not aligned, so we don't output anything
				begin
					illegalOperation = 1'b1;
					wantsInternalWE = 1'b0;
					latchInCurrentPC = 1'b0;
					rawDataIn  = 32'h00000000;
					rawAddress = 32'h00000000;
					dataOut = 32'h00000000;
				end
			end
			default://Should not ever happen
			begin
				illegalOperation = 1'b1;
				wantsInternalWE = 1'b0;
				latchInCurrentPC = 1'b0;
				rawDataIn  = 32'h00000000;
				rawAddress = 32'h00000000;
				dataOut = 32'h00000000;
			end
		endcase
	end
	else
	begin//we do nothing because memEnable is not enabled
		illegalOperation = 1'b0;
		wantsInternalWE = 1'b0;
		latchInCurrentPC = 1'b0;
		rawDataIn  = 32'h00000000;
		rawAddress = 32'h00000000;
		dataOut = 32'h00000000;
	end
end

//currentPC latching into tempAddressRegister if memOperationType = 2 and memEnable = 1
always @(posedge clock, posedge reset)
begin
	if (reset)
		tempAddressRegister <= 32'h00000000;
	else if (clock)
	begin
		if (memEnable && latchInCurrentPC)//mem is enabled and we want to latch in pc
			tempAddressRegister <= currentPC;
	end
end

//Read+modify write cycle logic (waits for rawDataOutput to update for 1 clock cycle then enables internalWE)
//If a store instruction is to be executed, wantsInternalWE is set on a negative edge because it is driven by combinational logic which is driven directly by the control logic which updates
//its output on negative edges. For sb and sw, the control logic holds the appropriate control lines for 2 clock cycles so therefore wantsInternalWE is held for 2 clock cycles (starting from a negedge)
//The below logic does not register wantsInternalWE on the first negedge because of contamination delay. This is intentional so that rawDataOutput has time to fetch old data
//from the address to be modified. rawDataOutput is latched internally inside of memoryBackend on the first posedge, then on the next negedge internalWE is set
//On the second posedge the modifed data is then written, then on the third negedge control changes to the next instruction disabling wantsInternalWE and immediatly
//disabling memoryBackend writing through isActuallyGoingToWrite (preventing writing twice). By the 4th posedge, internalWE is 0 and the modified data was written.
//Note: sw bypasses internalWE; see isActuallyGoingToWrite
always @(negedge clock, posedge reset)
begin
	if (reset)
	begin
		internalWE <= 0;
	end
	else if (~clock)
	begin
		if (wantsInternalWE)//because wantsInternalWE is set by combinational logic controlled by the control logic on the first negedge, internalWE is not actually flipped until the second negedge which is exactly what we need
		begin
			internalWE <= ~internalWE;
			memWriteReady <= ~memWriteReady;//todo ensure this is being set correctly
		end
		else
		begin
			memWriteReady <= 1;//todo ensure this is being set correctly
			internalWE <= 0;
		end
	end
end

/* Raw memory storage and endianness/io translation/helpers */
//Sign extention and zero extention of reordered/extracted data output//TODO create zero extention and sign extention functions in common functions file and use them here
wire [31:0] reorderedHighHalfwordOutSE = {{16{reorderedHighHalfwordOut[15]}}, reorderedHighHalfwordOut};//lh
wire [31:0] reorderedHighHalfwordOutZE = {16'h0000, reorderedHighHalfwordOut};//lhu
wire [31:0] reorderedLowHalfwordOutSE = {{16{reorderedLowHalfwordOut[15]}}, reorderedLowHalfwordOut};//lh
wire [31:0] reorderedLowHalfwordOutZE = {16'h0000, reorderedLowHalfwordOut};//lhu
wire [31:0] highByteOutSE = {{24{highByteOut[7]}}, highByteOut};//lb
wire [31:0] highByteOutZE = {24'h000000, highByteOut};//lbu
wire [31:0] highMidByteOutSE = {{24{highMidByteOut[7]}}, highMidByteOut};//lb
wire [31:0] highMidByteOutZE = {24'h000000, highMidByteOut};//lbu
wire [31:0] lowMidByteOutSE = {{24{lowMidByteOut[7]}}, lowMidByteOut};//lb
wire [31:0] lowMidByteOutZE = {24'h000000, lowMidByteOut};//lbu
wire [31:0] lowByteOutSE = {{24{lowByteOut[7]}}, lowByteOut};//lb
wire [31:0] lowByteOutZE = {24'h000000, lowByteOut};//lbu
//Conversion of rawDataOut to a properly ordered format for memory reads
wire [31:0] reorderedWordOut = toBigEndian(rawDataOut);//internal address offset must be 0; lw
wire [15:0] reorderedHighHalfwordOut = {rawDataOut[23:16], rawDataOut[31:24]};//internal address offset must be 0//TODO create a halfword endiannes function in the common functions file and use it here
wire [15:0] reorderedLowHalfwordOut = {rawDataOut[7:0], rawDataOut[15:8]};//internal address offset must be 2//TODO create a halfword endiannes function in the common functions file and use it here
wire [7:0] highByteOut = {rawDataOut[31:24]};//internal address offset must be 0
wire [7:0] highMidByteOut = {rawDataOut[23:16]};//internal address offset must be 1
wire [7:0] lowMidByteOut = {rawDataOut[15:8]};//internal address offset must be 2
wire [7:0] lowByteOut = {rawDataOut[7:0]};//internal address offset must be 3
//Reordering of portRS2 (which contains data to be writeen) to little endian format; also preserves original data where needed (use these by setting rawDataIn to one of these)
wire [31:0] reorderedWordIn = toLittleEndian(portRS2);//internalAddressOffset must be 0; sw
wire [31:0] reorderedHighHalfwordIn = {portRS2[7:0], portRS2[15:8], rawDataOut[15:0]};//internalAddressOffset must be 0; preserves bottom 2 bytes; sh//TODO create a halfword endiannes function in the common functions file and use it here
wire [31:0] reorderedLowHalfwordIn = {rawDataOut[31:16], portRS2[7:0], portRS2[15:8]};//internalAddressOffset must be 2; preserves upper 2 bytes; sh//TODO create a halfword endiannes function in the common functions file and use it here
wire [31:0] highByteIn = {portRS2[7:0], rawDataOut[23:0]};//internalAddressOffset must be 0; preserves lower 3 bytes; sb
wire [31:0] highMidByteIn = {rawDataOut[31:24], portRS2[7:0], rawDataOut[15:0]};//internalAddressOffset must be 1; preserves upper 1 and lower 2 bytes; sb
wire [31:0] lowMidByteIn = {rawDataOut[31:16], portRS2[7:0], rawDataOut[7:0]};//internalAddressOffset must be 2; preserves upper 2 and lower 1 bytes; sb
wire [31:0] lowByteIn = {rawDataOut[31:8], portRS2[7:0]};//internalAddressOffset must be 3; preserves upper 3 bytes; sb

//Actually deciding to write
//whole word stores only take 1 clock cycle because we don't care about the data that was there originally so we avoid waiting on the negedge logic above
//also if we disable wantsInternalWE on a negedge then internalWE is not disabled until the following negedge, so instead as soon as wantsInternalWE we disable the write_enable input immediatly
wire isActuallyGoingToWrite = (internalWE | (funct3 == 3'b010)) && wantsInternalWE;

//Memory backend (contains ram + initial contents of ram on fpga as well as memory mapped io)
memoryBackend #(.FILE(INITIAL_CONTENTS), .A_WIDTH(A_WIDTH)) memBackend
					(.clock(clock), .reset(reset), .write_enable(isActuallyGoingToWrite), .data_out(rawDataOut), .read_address(rawAddress), .data_in(rawDataIn), .write_address(rawAddress),
					 .mmPortA(mmPortA), .mmPortB(mmPortB), .mmPortC(mmPortC), .mmPortD(mmPortD), .mmPortADir(mmPortADir), .mmPortBDir(mmPortBDir), .mmPortCDir(mmPortCDir), .mmPortDDir(mmPortDDir));

//Include endianness functions from common functions file
`include "jzjcoreaCommonFunctions.v"

endmodule

module memoryBackend//Looks at address and enables the appropriate backend (memory, memory mapped io, etc)//Is purely combinational logic so there is no effect on speed
(
	input clock,
	input reset,
	
	input [31:0] data_in,
	input [31:0] write_address,//word-wise addressing, not bytewise (technically this is 4 times higher than risc v would allow but I'm ok with that)
	input write_enable,
	
	output reg [31:0] data_out,//this is a register data type but it is driven by combinational logic so don't worry
	input [31:0] read_address,//word-wise addressing, not bytewise (technically this is 4 times higher than risc v would allow but I'm ok with that)
	
	//CPU memory mapped io ports (for io dir, 0 = read, 1 = write)
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portX[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portX[0]
	//and for direction setting rs2[0] = portXDirMemAddress[24] = portXDirOfPin[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Addresses for port read/write   |   io direction (0 for input, 1 for output)
	inout [31:0] mmPortA,//FFFFFFF0   |   FFFFFFE0
	inout [31:0] mmPortB,//FFFFFFF4   |   FFFFFFE4
	inout [31:0] mmPortC,//FFFFFFF8   |   FFFFFFE8
	inout [31:0] mmPortD,//FFFFFFFC   |   FFFFFFEC
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems when compiling
	//portX[Y] can only be written to by an external moduleif it is not in output mode
	//if portXDir[y] is 1 then that pin is outputting data, if portXDir[y] is 0 then that pin is high impedance
	output [31:0] mmPortADir,
	output [31:0] mmPortBDir,
	output [31:0] mmPortCDir,
	output [31:0] mmPortDDir
);
//Parameters for ram
parameter FILE = "rom.mem";
parameter A_WIDTH = 12;

/* Ye ol' mess of wires */
//Backand Backend data outputs
wire [31:0] ramDataOut;
wire [31:0] mmIODataOut;

//Backend Backend write enables (register data types but driven by combinational logic so don't worry)
reg ramWE;
reg mmIOWE;

/* Addressing Logic */
//Address bounds (wordwise and inclusive)
localparam MIN_RAM_ADDRESS = 32'h00000000;//Starts at byte and word address 0
localparam MAX_RAM_ADDRESS = (2 ** A_WIDTH) - 1;//4096 words by default (16KiB = 16384 bytes total, therefore last address is from 16380 to 16383 (word-wise address 4095))
localparam MIN_MMIO_ADDRESS = 32'h3FFFFFF8;//Starts at byte address FFFFFFE0, which is word address 3FFFFFF8 (the first 4 bytes)
localparam MAX_MMIO_ADDRESS = 32'h3FFFFFFF;//Ends at byte address FFFFFFFF (last byte of word at byte address FFFFFFFC), which is word address 3FFFFFFF (the last 4 bytes)

//temporarly just sync ram is connected directly, but eventually memory addressing logic will go here
//assign data_out = ramDataOut;
//assign ramWE = write_enable;

//Reading (choose to multiplex one of several backend data outputs to data_out based on read_address)
always @*
begin
	if ((read_address >= MIN_RAM_ADDRESS) && (read_address <= MAX_RAM_ADDRESS))//ram
		data_out = ramDataOut;
	else if ((read_address >= MIN_MMIO_ADDRESS) && (read_address <= MAX_MMIO_ADDRESS))//memmory mapped io
		data_out = mmIODataOut;
	else//area that is not mapped
		data_out = 32'h00000000;
end

//Writing (choose to enable one of several backend write enables based on write_address)
always @*
begin
	if (write_enable)
	begin
		if ((write_address >= MIN_RAM_ADDRESS) && (write_address <= MAX_RAM_ADDRESS))//ram
		begin
			ramWE = 1'b1;
			mmIOWE = 1'b0;
		end
		else if ((write_address >= MIN_MMIO_ADDRESS) && (write_address <= MAX_MMIO_ADDRESS))//memmory mapped io
		begin
			ramWE = 1'b0;
			mmIOWE = 1'b1;
		end
		else//area that is not mapped
		begin
			ramWE = 1'b0;
			mmIOWE = 1'b0;
		end
	end
	else//not writing
	begin
		ramWE = 1'b0;
		mmIOWE = 1'b0;
	end
end

/* Memory Backend Backends */
//Syncronous ram
sync_ram #(.FILE(FILE), .A_WIDTH(A_WIDTH), .D_WIDTH(32), .INITIALIZE_FROM_FILE(1)) ram
			 (.clock(clock), .write_enable(ramWE), .data_out(ramDataOut), .read_address(read_address), .data_in(data_in), .write_address(write_address));
			  
//Memory mapped io
memoryMappedIOHandler mmIOHandler (.clock(clock), .reset(reset), .write_enable(mmIOWE), .data_out(mmIODataOut), .read_address(read_address), .data_in(data_in), .write_address(write_address),
												.portA(mmPortA), .portB(mmPortB), .portC(mmPortC), .portD(mmPortD), .portADir(mmPortADir), .portBDir(mmPortBDir), .portCDir(mmPortCDir),
												.portDDir(mmPortDDir));

endmodule

/* memoryBackend backend modules */

module memoryMappedIOHandler
(
	input clock,
	input reset,
	
	input [31:0] data_in,
	input [31:0] write_address,
	input write_enable,
	
	output reg [31:0] data_out,
	input [31:0] read_address,
	
	//CPU memory mapped rw ports (for io dir, 0 = read, 1 = write)
	//Word-wise addresses:						read/wri   |   io dir
	inout reg [31:0] portA,//portArray[0]:	3FFFFFFC   |   3FFFFFF8
	inout reg [31:0] portB,//portArray[1]:	3FFFFFFD   |   3FFFFFF9
	inout reg [31:0] portC,//portArray[2]:	3FFFFFFE   |   3FFFFFFA
	inout reg [31:0] portD,//portArray[3]:	3FFFFFFF   |   3FFFFFFB
	//inout reg [31:0] portArray [1:0]//Register 0 is portA read/write register, register 1 is portB read/write register, and so on//oops arrays need systemverilog extentions
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems when compiling
	//portX[Y] can only be written to by an external moduleif it is not in output mode
	//if portXDir[y] is 1 then that pin is outputting data, if portXDir[y] is 0 then that pin is high impedance
	output [31:0] portADir,
	output [31:0] portBDir,
	output [31:0] portCDir,
	output [31:0] portDDir
);
//Register Bank stuffs (note that registers in the register bank here are in big endian and must be converted for memory transferes)
reg [31:0] ioDirRegisterBank [3:0];//Register 0 is portA io direction, register 1 is portB io direction, and so on
assign portADir = ioDirRegisterBank[0];
assign portBDir = ioDirRegisterBank[1];
assign portCDir = ioDirRegisterBank[2];
assign portDDir = ioDirRegisterBank[3];

wire [1:0] ioDirRegisterBankReadAddress = read_address - 32'h3FFFFFF8;//subtracting word-wise address of lowest io dir register to get a range from 0 to 3 (a to d)
wire [1:0] ioDirRegisterBankWriteAddress = write_address - 32'h3FFFFFF8;//subtracting word-wise address of lowest io dir register to get a range from 0 to 3 (a to d)
wire [1:0] rwRegisterReadNumber = read_address - 32'h3FFFFFFC;//subtracting word-wise address of lowest portArray register to get a range from 0 to 3 (a to d)
wire [1:0] rwRegisterWriteNumber = write_address - 32'h3FFFFFFC;//subtracting word-wise address of lowest  portArray register to get a range from 0 to 3 (a to d)

wire [2:0] overallRegisterBankReadAddress = read_address - 32'h3FFFFFF8;//usefull for the wires below for telling is io dir registers or port registers are active for reading
wire [2:0] overallRegisterBankWriteAddress = write_address - 32'h3FFFFFF8;//usefull for the wires below for telling is io dir registers or port registers are active for writing
wire isReadFromIODirRegister = overallRegisterBankReadAddress < 4;//1 if a read_address indicates a read from one of the ioDirRegisters
wire isWriteToIODirRegister = overallRegisterBankWriteAddress < 4;//1 if a read_address indicates a write to one of the ioDirRegisters
wire isReadFromPortRegister = overallRegisterBankReadAddress >= 4;//1 if a read_address indicates a read from one of the registers in portArray
wire isWriteToPortRegister = overallRegisterBankReadAddress >= 4;//1 if a read_address indicates a write to one of the registers in portArray

//Reading from a register (latches data into data_out)
always @(posedge clock, posedge reset)
begin
	if (reset)
		data_out <= 32'h00000000;
	else if (clock)
	begin
		if (isReadFromIODirRegister)
			data_out <= toLittleEndian(ioDirRegisterBank[ioDirRegisterBankReadAddress]);
		else if (isReadFromPortRegister)
		begin
			case (rwRegisterReadNumber)
				0: data_out <= toLittleEndian(portA);
				1: data_out <= toLittleEndian(portB);
				2: data_out <= toLittleEndian(portC);
				3: data_out <= toLittleEndian(portD);
				default: data_out <= 32'h00000000;
			endcase
		end
		else
			data_out <= 32'h00000000;
	end
end

//Writing (writes to io dir registers unconditionally, but only writes a bit to a port if the appropriate io dir for that port is set (otherwise it writes z))
//If a 0 is written to an io dir register, the corresponding bit is set to z in the portArray register
always @(posedge clock, posedge reset)
begin
	if (reset)
		initRegisters();
	else if (clock)
	begin
		if (write_enable)
		begin
			if (isWriteToIODirRegister)
			begin
				case (ioDirRegisterBankWriteAddress)
					0://portA io dir
					begin
						ioDirRegisterBank[0] <= toBigEndian(data_in);
						portA <= updatedPortRegister(portA, toBigEndian(data_in));//Update port A based on changes to ioDirRegisterBank[0]
					end
					1://portB io dir
					begin
						ioDirRegisterBank[1] <= toBigEndian(data_in);
						portB <= updatedPortRegister(portB, toBigEndian(data_in));//Update port B based on changes to ioDirRegisterBank[1]
					end
					2://portC io dir
					begin
						ioDirRegisterBank[2] <= toBigEndian(data_in);
						portC <= updatedPortRegister(portC, toBigEndian(data_in));//Update port C based on changes to ioDirRegisterBank[2]
					end
					3://portD io dir
					begin
						ioDirRegisterBank[3] <= toBigEndian(data_in);
						portD <= updatedPortRegister(portD, toBigEndian(data_in));//Update port D based on changes to ioDirRegisterBank[3]
					end
				endcase
			end
			else if (isWriteToPortRegister)
			begin
				case (rwRegisterWriteNumber)
					0: portA <= updatedPortRegister(toBigEndian(data_in), ioDirRegisterBank[0]);//Update portA taking into account the io dir for each bit
					1: portB <= updatedPortRegister(toBigEndian(data_in), ioDirRegisterBank[1]);//Update portB taking into account the io dir for each bit
					2: portC <= updatedPortRegister(toBigEndian(data_in), ioDirRegisterBank[2]);//Update portC taking into account the io dir for each bit
					3: portD <= updatedPortRegister(toBigEndian(data_in), ioDirRegisterBank[3]);//Update portC taking into account the io dir for each bit
				endcase
			end
		end
	end
end

//Enabling/disabling outputs based on ioDirRegister
function automatic [31:0] updatedPortRegister(input [31:0] rawPortRegister, input [31:0] ioDirRegister);//If a bit in the io dir register is 0, then the corresponding bit in updatedPortRegister is set to z. Otherwise it is passed through
integer i;
begin
	for (i = 0; i < 32; i = i + 1)
	begin
        updatedPortRegister[i] = ioDirRegister[i] ? rawPortRegister[i] : 1'bz;//If the ioDirectionRegister is set to 1, then we are writing the data in rawPortRegister, otherwise we are reading by first setting the register to z
	end
end
endfunction

//Initial register values
initial
begin
	initRegisters();
end

//Set all io dir registers to 0 and set all port registers to z
task initRegisters();//Intentionally not automatic because the only things that use it are the initial block and reset inside of the write block
integer i;
begin
	//Set all io dir registers to 0
	for (i = 0; i < 4; i = i + 1)
	begin
		ioDirRegisterBank[i] <= 32'h00000000;
	end
	
	//Set all port registers to z
	portA <= 32'hzzzzzzzz;
	portB <= 32'hzzzzzzzz;
	portC <= 32'hzzzzzzzz;
	portD <= 32'hzzzzzzzz;
end
endtask

//Include endianness functions from common functions file
`include "jzjcoreaCommonFunctions.v"

endmodule

//I wrote this before. Pretty reliable inferred ram module but probably only works on fpgas because of readmemb/readmemh
module sync_ram//dual port
(
	input clock,
	
	input [D_MAX:0] data_in,
	input [A_MAX:0] write_address,
	input write_enable,
	
	output reg [D_MAX:0] data_out,
	input [A_MAX:0] read_address
);
//based on https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/qts/qts_qii51007.pdf

parameter INITIALIZE_FROM_FILE = 0;//whether to have default ram contents at boot
parameter FILE = "rom.mem";
parameter FILE_TYPE_BIN = 0;//hex by default
parameter D_WIDTH = 8;
parameter A_WIDTH = 8;

localparam D_MAX = D_WIDTH - 1;
localparam A_MAX = A_WIDTH - 1;
localparam NUM_OF_ADDRESSES = 2 ** A_WIDTH;
localparam NUM_OF_ADDRESSES_MAX = NUM_OF_ADDRESSES - 1;

reg [D_MAX:0] ram [NUM_OF_ADDRESSES_MAX:0];

initial
begin
	if (INITIALIZE_FROM_FILE)
	begin
		if (FILE_TYPE_BIN)
		  $readmemb(FILE, ram);
		else
		  $readmemh(FILE, ram);
	end
end

always @(posedge clock)
begin
	if (write_enable)
		ram[write_address] <= data_in;
	
	data_out <= ram[read_address];//will have old values until next clock cycle
end

endmodule