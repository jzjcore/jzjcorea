module registerFileV2
(
	input clock,
	input reset,
	
	input [4:0] registerWriteSelect,//registers 0 to 31
	input [4:0] registerOutputSelect,//registers 0 to 31
	input writeEnable,
	
	input [31:0] busIO,//used to write to registers
	
	//Read ports
	input [4:0] portRS1RegSelect,//rs1
	input [4:0] portRS2RegSelect,//rs2
	output [31:0] portRS1,//rs1
	output [31:0] portRS2,//rs2
	
	//Output for legacy asembly test programs that output to register 31; will be removed in later versions of jzjcore
	output [31:0] register31Output
);
//Direct outputs for portRS1, portRS2 and register31Output (reading)
assign portRS1 = (portRS1RegSelect == 5'b00000) ? 32'h00000000 : actualRegisterFile[portRS1RegSelect];//If the port is looking at x0 then we return all 0s
assign portRS2 = (portRS2RegSelect == 5'b00000) ? 32'h00000000 : actualRegisterFile[portRS2RegSelect];//If the port is looking at x0 then we return all 0s
assign register31Output = actualRegisterFile[31];//Output for legacy asembly test programs that output to register 31; will be removed in later versions of jzjcore

//The registers
reg [31:0] actualRegisterFile[31:1];//x0 is never written to so there is no reason to dedicate an actual register to it

//Writing logic
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		initRegisters();
	end
	else if (clock)
	begin
		if (writeEnable && (registerWriteSelect != 5'b00000))//Write the appropriate register, ignoring writes to x0
			actualRegisterFile[registerWriteSelect] <= busIO;//read new data off the bus into the register
	end
end

//Initialize registers
initial
begin
	initRegisters();
end

//Sets all registers to 0
task initRegisters();//Intentionally not automatic because the only things that use it are the initial block and reset inside of the write block
integer i;
begin
	//Initialze registers to 0
	for (i = 1; i < 32; i = i + 1)
	begin
		actualRegisterFile[i] <= 32'h00000000;
	end
end
endtask

endmodule