module registerFile
(
	input clock,
	input reset,
	
	input [4:0] registerWriteSelect,//registers 0 to 31
	input [4:0] registerOutputSelect,//registers 0 to 31
	input writeEnable,
	input outputEnable,
	
	inout [31:0] busIO,//driven by/read by registers
	
	//Side ports (always active)
	input [4:0] portRS1RegSelect,//rs1
	input [4:0] portRS2RegSelect,//rs2
	output [31:0] portRS1,//rs1
	output [31:0] portRS2,//rs2
	
	//Output for legacy asembly test programs that output to register 31; will be removed in later versions of jzjcore
	output [31:0] register31Output
);
reg [31:0] registerWE = 32'h00000000;//Demultiplexed write enable lines
reg [31:0] registerOE = 32'h00000000;//Demultiplexed output enable lines
wire [31:0] currentDataInRegisters[31:0];//todo use for direct register reading (some instructions from alu that way it won't have to use the bus)

//Demultiplexer for writeEnable
always @*
begin
	registerWE = 32'h00000000;//set all lines to 0 so only 1 line is set at a time and to avoid latches
	
	if (writeEnable)
	begin
		case (registerWriteSelect)
			5'b00000 : registerWE[0] = 1'b1;//x0 (this is ignored by the register though)
			5'b00001 : registerWE[1] = 1'b1;//x1
			5'b00010 : registerWE[2] = 1'b1;//x2
			5'b00011 : registerWE[3] = 1'b1;//x3
			5'b00100 : registerWE[4] = 1'b1;//x4
			5'b00101 : registerWE[5] = 1'b1;//x5
			5'b00110 : registerWE[6] = 1'b1;//x6
			5'b00111 : registerWE[7] = 1'b1;//x7
			5'b01000 : registerWE[8] = 1'b1;//x8
			5'b01001 : registerWE[9] = 1'b1;//x9
			5'b01010 : registerWE[10] = 1'b1;//x10
			5'b01011 : registerWE[11] = 1'b1;//x11
			5'b01100 : registerWE[12] = 1'b1;//x12
			5'b01101 : registerWE[13] = 1'b1;//x13
			5'b01110 : registerWE[14] = 1'b1;//x14
			5'b01111 : registerWE[15] = 1'b1;//x15
			5'b10000 : registerWE[16] = 1'b1;//x16
			5'b10001 : registerWE[17] = 1'b1;//x17
			5'b10010 : registerWE[18] = 1'b1;//x18
			5'b10011 : registerWE[19] = 1'b1;//x19
			5'b10100 : registerWE[20] = 1'b1;//x20
			5'b10101 : registerWE[21] = 1'b1;//x21
			5'b10110 : registerWE[22] = 1'b1;//x22
			5'b10111 : registerWE[23] = 1'b1;//x23
			5'b11000 : registerWE[24] = 1'b1;//x24
			5'b11001 : registerWE[25] = 1'b1;//x25
			5'b11010 : registerWE[26] = 1'b1;//x26
			5'b11011 : registerWE[27] = 1'b1;//x27
			5'b11100 : registerWE[28] = 1'b1;//x28
			5'b11101 : registerWE[29] = 1'b1;//x29
			5'b11110 : registerWE[30] = 1'b1;//x30
			5'b11111 : registerWE[31] = 1'b1;//x31
			default : registerWE = 32'h00000000;//Can never happen, but is good practice to avoid latches
		endcase
	end
end

//Demultiplexer for outputEnable
always @*
begin
	registerOE = 32'h00000000;//set all lines to 0 so only 1 line is set at a time and to avoid latches
	
	if (outputEnable)
	begin
		case (registerOutputSelect)
			5'b00000 : registerOE[0] = 1'b1;//x0
			5'b00001 : registerOE[1] = 1'b1;//x1
			5'b00010 : registerOE[2] = 1'b1;//x2
			5'b00011 : registerOE[3] = 1'b1;//x3
			5'b00100 : registerOE[4] = 1'b1;//x4
			5'b00101 : registerOE[5] = 1'b1;//x5
			5'b00110 : registerOE[6] = 1'b1;//x6
			5'b00111 : registerOE[7] = 1'b1;//x7
			5'b01000 : registerOE[8] = 1'b1;//x8
			5'b01001 : registerOE[9] = 1'b1;//x9
			5'b01010 : registerOE[10] = 1'b1;//x10
			5'b01011 : registerOE[11] = 1'b1;//x11
			5'b01100 : registerOE[12] = 1'b1;//x12
			5'b01101 : registerOE[13] = 1'b1;//x13
			5'b01110 : registerOE[14] = 1'b1;//x14
			5'b01111 : registerOE[15] = 1'b1;//x15
			5'b10000 : registerOE[16] = 1'b1;//x16
			5'b10001 : registerOE[17] = 1'b1;//x17
			5'b10010 : registerOE[18] = 1'b1;//x18
			5'b10011 : registerOE[19] = 1'b1;//x19
			5'b10100 : registerOE[20] = 1'b1;//x20
			5'b10101 : registerOE[21] = 1'b1;//x21
			5'b10110 : registerOE[22] = 1'b1;//x22
			5'b10111 : registerOE[23] = 1'b1;//x23
			5'b11000 : registerOE[24] = 1'b1;//x24
			5'b11001 : registerOE[25] = 1'b1;//x25
			5'b11010 : registerOE[26] = 1'b1;//x26
			5'b11011 : registerOE[27] = 1'b1;//x27
			5'b11100 : registerOE[28] = 1'b1;//x28
			5'b11101 : registerOE[29] = 1'b1;//x29
			5'b11110 : registerOE[30] = 1'b1;//x30
			5'b11111 : registerOE[31] = 1'b1;//x31
			default : registerWE = 32'h00000000;//Can never happen, but is good practice to avoid latches
		endcase
	end
end

//Demux aluPortX and aluPortY
//todo this is more efficient than register modules; convert registers to use a verilog array in later revisions instead of individual modules...
//...like this array of currentDataInRegisters
assign portRS1 = currentDataInRegisters[portRS1RegSelect];
assign portRS2 = currentDataInRegisters[portRS2RegSelect];

//Instantize register modules (todo use verilog arrays and a single always block in this module instead for more effecient lut use)
registerZero x0 (.clock(clock), .reset(reset), .outputEnable(registerOE[0]), .busIO(busIO), .currentData(currentDataInRegisters[0]));
register x1 (.clock(clock), .reset(reset), .writeEnable(registerWE[1]), .outputEnable(registerOE[1]), .busIO(busIO), .currentData(currentDataInRegisters[1]));
register x2 (.clock(clock), .reset(reset), .writeEnable(registerWE[2]), .outputEnable(registerOE[2]), .busIO(busIO), .currentData(currentDataInRegisters[2]));
register x3 (.clock(clock), .reset(reset), .writeEnable(registerWE[3]), .outputEnable(registerOE[3]), .busIO(busIO), .currentData(currentDataInRegisters[3]));
register x4 (.clock(clock), .reset(reset), .writeEnable(registerWE[4]), .outputEnable(registerOE[4]), .busIO(busIO), .currentData(currentDataInRegisters[4]));
register x5 (.clock(clock), .reset(reset), .writeEnable(registerWE[5]), .outputEnable(registerOE[5]), .busIO(busIO), .currentData(currentDataInRegisters[5]));
register x6 (.clock(clock), .reset(reset), .writeEnable(registerWE[6]), .outputEnable(registerOE[6]), .busIO(busIO), .currentData(currentDataInRegisters[6]));
register x7 (.clock(clock), .reset(reset), .writeEnable(registerWE[7]), .outputEnable(registerOE[7]), .busIO(busIO), .currentData(currentDataInRegisters[7]));
register x8 (.clock(clock), .reset(reset), .writeEnable(registerWE[8]), .outputEnable(registerOE[8]), .busIO(busIO), .currentData(currentDataInRegisters[8]));
register x9 (.clock(clock), .reset(reset), .writeEnable(registerWE[9]), .outputEnable(registerOE[9]), .busIO(busIO), .currentData(currentDataInRegisters[9]));
register x10 (.clock(clock), .reset(reset), .writeEnable(registerWE[10]), .outputEnable(registerOE[10]), .busIO(busIO), .currentData(currentDataInRegisters[10]));
register x11 (.clock(clock), .reset(reset), .writeEnable(registerWE[11]), .outputEnable(registerOE[11]), .busIO(busIO), .currentData(currentDataInRegisters[11]));
register x12 (.clock(clock), .reset(reset), .writeEnable(registerWE[12]), .outputEnable(registerOE[12]), .busIO(busIO), .currentData(currentDataInRegisters[12]));
register x13 (.clock(clock), .reset(reset), .writeEnable(registerWE[13]), .outputEnable(registerOE[13]), .busIO(busIO), .currentData(currentDataInRegisters[13]));
register x14 (.clock(clock), .reset(reset), .writeEnable(registerWE[14]), .outputEnable(registerOE[14]), .busIO(busIO), .currentData(currentDataInRegisters[14]));
register x15 (.clock(clock), .reset(reset), .writeEnable(registerWE[15]), .outputEnable(registerOE[15]), .busIO(busIO), .currentData(currentDataInRegisters[15]));
register x16 (.clock(clock), .reset(reset), .writeEnable(registerWE[16]), .outputEnable(registerOE[16]), .busIO(busIO), .currentData(currentDataInRegisters[16]));
register x17 (.clock(clock), .reset(reset), .writeEnable(registerWE[17]), .outputEnable(registerOE[17]), .busIO(busIO), .currentData(currentDataInRegisters[17]));
register x18 (.clock(clock), .reset(reset), .writeEnable(registerWE[18]), .outputEnable(registerOE[18]), .busIO(busIO), .currentData(currentDataInRegisters[18]));
register x19 (.clock(clock), .reset(reset), .writeEnable(registerWE[19]), .outputEnable(registerOE[19]), .busIO(busIO), .currentData(currentDataInRegisters[19]));
register x20 (.clock(clock), .reset(reset), .writeEnable(registerWE[20]), .outputEnable(registerOE[20]), .busIO(busIO), .currentData(currentDataInRegisters[20]));
register x21 (.clock(clock), .reset(reset), .writeEnable(registerWE[21]), .outputEnable(registerOE[21]), .busIO(busIO), .currentData(currentDataInRegisters[21]));
register x22 (.clock(clock), .reset(reset), .writeEnable(registerWE[22]), .outputEnable(registerOE[22]), .busIO(busIO), .currentData(currentDataInRegisters[22]));
register x23 (.clock(clock), .reset(reset), .writeEnable(registerWE[23]), .outputEnable(registerOE[23]), .busIO(busIO), .currentData(currentDataInRegisters[23]));
register x24 (.clock(clock), .reset(reset), .writeEnable(registerWE[24]), .outputEnable(registerOE[24]), .busIO(busIO), .currentData(currentDataInRegisters[24]));
register x25 (.clock(clock), .reset(reset), .writeEnable(registerWE[25]), .outputEnable(registerOE[25]), .busIO(busIO), .currentData(currentDataInRegisters[25]));
register x26 (.clock(clock), .reset(reset), .writeEnable(registerWE[26]), .outputEnable(registerOE[26]), .busIO(busIO), .currentData(currentDataInRegisters[26]));
register x27 (.clock(clock), .reset(reset), .writeEnable(registerWE[27]), .outputEnable(registerOE[27]), .busIO(busIO), .currentData(currentDataInRegisters[27]));
register x28 (.clock(clock), .reset(reset), .writeEnable(registerWE[28]), .outputEnable(registerOE[28]), .busIO(busIO), .currentData(currentDataInRegisters[28]));
register x29 (.clock(clock), .reset(reset), .writeEnable(registerWE[29]), .outputEnable(registerOE[29]), .busIO(busIO), .currentData(currentDataInRegisters[29]));
register x30 (.clock(clock), .reset(reset), .writeEnable(registerWE[30]), .outputEnable(registerOE[30]), .busIO(busIO), .currentData(currentDataInRegisters[30]));
register x31 (.clock(clock), .reset(reset), .writeEnable(registerWE[31]), .outputEnable(registerOE[31]), .busIO(busIO), .currentData(currentDataInRegisters[31]));

//Output for legacy asembly test programs that output to register 31; will be removed in later versions of jzjcore
assign register31Output = currentDataInRegisters[31];

endmodule

/* Register Modules */
module register
(
	input clock,
	input reset,
	
	input writeEnable,
	input outputEnable,
	
	inout reg [31:0] busIO = 32'bz,
	
	output [31:0] currentData//usefull for the alu to fetch data from the register directly for certain instructions
);

reg [31:0] data = 32'h00000000;//start with nothing in it
assign currentData = data;

always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		data <= 32'h00000000;//empty register
		busIO <= 32'bz;//take output off the bus
	end
	else if (clock)
	begin
		if (writeEnable)
		begin
			data <= busIO;
		end
		else if (outputEnable)
		begin
			busIO <= data;
		end
		else
		begin
			busIO <= 32'bz;//take output off the bus
		end
	end
end

endmodule

module registerZero
(
	input clock,
	input reset,
	
	input outputEnable,
	
	inout reg [31:0] busIO = 32'bz,
	
	output [31:0] currentData//usefull for the alu to fetch data from the register directly for certain instructions
);

assign currentData = 32'h00000000;//x0 is always 0

always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		busIO <= 32'bz;//take output off the bus
	end
	else if (clock)
	begin
		if (outputEnable)
		begin
			busIO <= 32'h00000000;//x0 is always 0
		end
		else
		begin
			busIO <= 32'bz;//take output off the bus
		end
	end
end

endmodule