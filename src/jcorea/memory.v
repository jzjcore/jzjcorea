module memory
(
	input clock,
	input reset,
	
	inout reg [31:0] bus = 32'hz,
	
	//Status signals for control logic
	output memReadReady,
	output memWriteReady,
	output reg illegalOperation = 1'b0,//read/write not aligned (word to 4 multiple address, half word to 2 multiple address)
	
	//should never both be 1; nothing should be done unless one of these is active
	input memWE,//interpret funct3 as a store type instruction and read from portRS2 to store into memory at address immediateS + portRS1
	input memOE,//interpret funct3 as a load type instruction and read from address immediateI + portRS1 and output the proper value on the bus
	
	input [2:0] funct3,//used to deside between lw/lh/lhu/lb/lbu if memOE == 1, or between sw/sh/sb if memWE == 1
	
	//Values
	input [31:0] portRS1,//rs1
	input [31:0] portRS2,//rs2
	input [31:0] immediateI,//offset for loads
	input [31:0] immediateS,//offset for stores
	
	//Instruction fetching
	input latchInPC,//latch in current value of program counter
	input memPCOE,//look at the latched program register as an address instead of portRS1 + immediateI
	input [31:0] currentPC,
	
	//testing
	output [31:0] testOutput
);

assign memReadReady = 1'b1;//always ready for another read command because reads only take 1 cycle useful for future modules that have delays for some reason (eg. external ram)
assign memWriteReady = 1'b1;//always ready for another write command because writes only take 1 cycle; useful for future modules that have delays for some reason (eg. external ram)

//Temporary testing (works for LW instruction kind of)
assign testOutput = programCounterAddress;
/*
assign testOutput = testReorderedDataOut;
wire [31:0] testReorderedDataOut = {rawDataOut[7:0], rawDataOut[15:8], rawDataOut[23:16], rawDataOut[31:24]};//this actually might be useful in the actual module
assign rawReadAddress = memPCOE ? programCounterAddress : (portRS1 + immediateI);//this actually might be useful in the actual module
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		memReadReady <= 1'b1;
		memWriteReady <= 1'b1;
		bus <= 32'hz;
	end
	else if (clock)
	begin
		if (memPCOE)
		begin
			bus <= testReorderedDataOut;//output data at pc address to bus
		end
		else if (memOE)//only do lw instructions and assume all mem commands are lw
		begin
			bus <= testReorderedDataOut;//output data at address to bus
		end
		else
		begin
			memReadReady <= 1'b1;
			memWriteReady <= 1'b1;
			bus <= 32'hz;
		end
	end
end*/

//Module is ordered from raw memory management to higher level bus management
parameter INITIAL_CONTENTS = "rom.mem";
localparam A_WIDTH = 12;//My fpga only has 276480 bits of memory and I don't want to use it all up. An external memory controler is a task/project for another day
localparam D_WIDTH = 32;

wire writing = memWE;
wire reading = memOE | memPCOE;

reg [31:0] dataOut;
//Bus IO and clock stuffs (manages output of dataOut)
always @(posedge clock, posedge reset)
begin
	if (reset)
		bus <= 32'hz;
	else if (clock)
	begin
		if (reading && !illegalOperation)//things are good!
			bus <= dataOut;
		else
			bus <= 32'hz;
	end
	//todo (also be sure to check invalidOperation and not output to bus if it is 1)
end

//PC storage register and register
//used for fetch step in instruction cycle, not for instructions
reg [31:0] programCounterAddress;
always @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		programCounterAddress <= 32'b00000000;
	end
	else if (clock)
	begin
		if (latchInPC)
		begin
			programCounterAddress <= currentPC;
		end
	end
end

//Funct3, internalAddressOffset, and WE/OE interpreting (selects what rawDataIn is, what dataOut is, and if illegalOperation is 1 or not)
always @*
begin
	if (writing)
	begin
		dataOut = 32'h00000000;//we're not reading
		
		case (funct3)
			3'b000://sb
			begin
				illegalOperation = 1'b0;//No problems so far
			
				//Choose which byte from the 32 bit physical data storage to put into dataOut
				if (internalAddressOffset == 0)
					rawDataIn = highByteIn;
				else if (internalAddressOffset == 1)
					rawDataIn = highMidByteIn;
				else if (internalAddressOffset == 2)
					rawDataIn = lowMidByteIn;
				else if (internalAddressOffset == 3)
					rawDataIn = lowByteIn;
				else//should never happen
				begin
					rawDataIn = rawDataOut;//Prevent a write from changing anything
					illegalOperation = 1'b1;
				end
			end
			3'b001://sh
			begin
				illegalOperation = 1'b0;//No problems so far
			
				//Choose which half word from the 32 bit physical data storage to put into dataOut
				if (internalAddressOffset == 0)
					rawDataIn = reorderedHighHalfwordIn;
				else if (internalAddressOffset == 2)
					rawDataIn = reorderedLowHalfwordIn;
				else
				begin
					rawDataIn = rawDataOut;//Prevent a write from changing anything
					illegalOperation = 1'b1;//half word not aligned
				end
			end
			3'b010://sw
			begin
				if (internalAddressOffset == 0)
				begin
					rawDataIn = reorderedWordIn;
					illegalOperation = 1'b0;
				end
				else
				begin
					rawDataIn = rawDataOut;//Prevent a write from changing anything
					illegalOperation = 1'b1;//not aligned
				end
			end
			default:
			begin
				rawDataIn = rawDataOut;//Prevent a write from changing anything
				illegalOperation = 1'b1;//bad funct3
			end
		endcase
	end
	else if (memOE)
	begin
		rawDataIn = rawDataOut;//we're not writing; Prevent a write from changing anything
	
		case (funct3)
			3'b000://lb
			begin
				illegalOperation = 1'b0;//No problems so far
			
				//Choose which byte from the 32 bit physical data storage to put into dataOut
				if (internalAddressOffset == 0)
					dataOut = highByteOutSE;
				else if (internalAddressOffset == 1)
					dataOut = highMidByteOutSE;
				else if (internalAddressOffset == 2)
					dataOut = lowMidByteOutSE;
				else if (internalAddressOffset == 3)
					dataOut = lowByteOutSE;
				else//should never happen
				begin
					dataOut = 32'h00000000;
					illegalOperation = 1'b1;
				end
			end
			3'b001://lh
			begin
				illegalOperation = 1'b0;//No problems so far
			
				//Choose which half word from the 32 bit physical data storage to put into dataOut
				if (internalAddressOffset == 0)
					dataOut = reorderedHighHalfwordOutSE;
				else if (internalAddressOffset == 2)
					dataOut = reorderedLowHalfwordOutSE;
				else
				begin
					dataOut = 32'h00000000;
					illegalOperation = 1'b1;//half word not aligned
				end
			end
			3'b010://lw
			begin
				if (internalAddressOffset == 0)
				begin
					dataOut = reorderedWordOut;
					illegalOperation = 1'b0;
				end
				else
				begin
					dataOut = 32'h00000000;//not aligned
					illegalOperation = 1'b1;
				end
			end
			3'b100://lbu
			begin
				illegalOperation = 1'b0;//No problems so far
			
				//Choose which byte from the 32 bit physical data storage to put into dataOut
				if (internalAddressOffset == 0)
					dataOut = highByteOutZE;
				else if (internalAddressOffset == 1)
					dataOut = highMidByteOutZE;
				else if (internalAddressOffset == 2)
					dataOut = lowMidByteOutZE;
				else if (internalAddressOffset == 3)
					dataOut = lowByteOutZE;
				else//should never happen
				begin
					dataOut = 32'h00000000;
					illegalOperation = 1'b1;
				end
			end
			3'b101://lhu
			begin
				illegalOperation = 1'b0;//No problems so far
				
				//Choose which half word from the 32 bit physical data storage to put into dataOut
				if (internalAddressOffset == 0)
					dataOut = reorderedHighHalfwordOutZE;
				else if (internalAddressOffset == 2)
					dataOut = reorderedLowHalfwordOutZE;
				else
				begin
					dataOut = 32'h00000000;
					illegalOperation = 1'b1;//half word not aligned
				end
			end
			default:
			begin
				dataOut = 32'h00000000;
				illegalOperation = 1'b1;//bad funct3
			end
		endcase
	end
	else if (memPCOE)
	begin
		rawDataIn = rawDataOut;//we're not writing; Prevent a write from changing anything
	
		if (internalAddressOffset == 0)//instruction aligned
		begin
			dataOut = reorderedWordOut;
			illegalOperation = 1'b0;
		end
		else
		begin
			dataOut = 32'h00000000;
			illegalOperation = 1'b1;
		end
	end
	else
	begin
		dataOut = 32'h00000000;
		rawDataIn = rawDataOut;//Prevent a write from changing anything
		illegalOperation = 1'b0;//can't be doing something wrong if we're not doing anything at all
	end
end

//Addressing
//If we want to get the address in the mar (containing the program counter address for a fetch cycle) then we use that register instead of rs1 + immediate
wire [31:0] immediate = (memOE && !memWE) ? immediateI : immediateS;
wire [31:0] inputAddress = memPCOE ? programCounterAddress : (portRS1 + immediate);
wire [31:0] actualAddress = inputAddress / 4;//The sync ram module is configured with 32 bits of data per address, so every risc v "address" needs to be divided by 4
wire [1:0] internalAddressOffset = inputAddress % 4;//Which byte in the 32 bit data at the address we should read from

//Sign extention (SE) / zero entention (ZE) for reads (dataOut can be directly set to any of these)
wire [31:0] reorderedHighHalfwordOutSE = {{16{reorderedHighHalfwordOut[15]}}, reorderedHighHalfwordOut};
wire [31:0] reorderedHighHalfwordOutZE = {16'h0000, reorderedHighHalfwordOut};
wire [31:0] reorderedLowHalfwordOutSE = {{16{reorderedLowHalfwordOut[15]}}, reorderedLowHalfwordOut};
wire [31:0] reorderedLowHalfwordOutZE = {16'h0000, reorderedLowHalfwordOut};
wire [31:0] highByteOutSE = {{24{highByteOut[7]}}, highByteOut};
wire [31:0] highByteOutZE = {24'h000000, highByteOut};
wire [31:0] highMidByteOutSE = {{24{highMidByteOut[7]}}, highMidByteOut};
wire [31:0] highMidByteOutZE = {24'h000000, highMidByteOut};
wire [31:0] lowMidByteOutSE = {{24{lowMidByteOut[7]}}, lowMidByteOut};
wire [31:0] lowMidByteOutZE = {24'h000000, lowMidByteOut};
wire [31:0] lowByteOutSE = {{24{lowByteOut[7]}}, lowByteOut};
wire [31:0] lowByteOutZE = {24'h000000, lowByteOut};

//Byte rearanging/management (little endian storage)
//Loads (dataOut can only directly be set to reorderedWordOut; half words and bytes need sign/zero extention above)
wire [31:0] reorderedWordOut = {rawDataOut[7:0], rawDataOut[15:8], rawDataOut[23:16], rawDataOut[31:24]};//internalAddressOffset must be 0
wire [15:0] reorderedHighHalfwordOut = {rawDataOut[23:16], rawDataOut[31:24]};//internalAddressOffset must be 0
wire [15:0] reorderedLowHalfwordOut = {rawDataOut[7:0], rawDataOut[15:8]};//internalAddressOffset must be 2
wire [7:0] highByteOut = {rawDataOut[31:24]};//internalAddressOffset must be 0
wire [7:0] highMidByteOut = {rawDataOut[23:16]};//internalAddressOffset must be 1
wire [7:0] lowMidByteOut = {rawDataOut[15:8]};//internalAddressOffset must be 2
wire [7:0] lowByteOut = {rawDataOut[7:0]};//internalAddressOffset must be 3
//Stores (rawDataIn can be directly set to any of these)
wire [31:0] reorderedWordIn = {portRS2[7:0], portRS2[15:8], portRS2[23:16], portRS2[31:24]};//internalAddressOffset must be 0
wire [31:0] reorderedHighHalfwordIn = {portRS2[7:0], portRS2[15:8], rawDataOut[15:0]};//internalAddressOffset must be 0; preserves bottom 2 bytes
wire [31:0] reorderedLowHalfwordIn = {rawDataOut[31:16], portRS2[7:0], portRS2[15:8]};//internalAddressOffset must be 2; preserves upper 2 bytes
wire [31:0] highByteIn = {portRS2[31:24], rawDataOut[23:0]};//internalAddressOffset must be 0; preserves lower 3 bytes
wire [31:0] highMidByteIn = {rawDataOut[31:24], portRS2[23:16], rawDataOut[15:0]};//internalAddressOffset must be 1; preserves upper 1 and lower 2 bytes
wire [31:0] lowMidByteIn = {rawDataOut[31:16], portRS2[15:8], rawDataOut[7:0]};//internalAddressOffset must be 2; preserves upper 2 and lower 1 bytes
wire [31:0] lowByteIn = {rawDataOut[31:8], portRS2[7:0]};//internalAddressOffset must be 3; preserves upper 3 bytes

//little endian/raw storage and addressing for the sync_ram
reg [31:0] rawDataIn;
wire [31:0] rawDataOut;
wire [31:0] rawReadAddress = actualAddress;
wire [31:0] rawWriteAddress = actualAddress;
wire internalWE = writing && !illegalOperation;//we don't want to write if something is not going right (hey that rhymes)
sync_ram #(.FILE(INITIAL_CONTENTS), .A_WIDTH(A_WIDTH), .D_WIDTH(D_WIDTH), .INITIALIZE_FROM_FILE(1)) (.clock(clock), .write_enable(internalWE),
			  .data_out(rawDataOut), .read_address(rawReadAddress), .data_in(rawDataIn), .write_address(rawWriteAddress));

endmodule
