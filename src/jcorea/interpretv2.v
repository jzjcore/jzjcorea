module instructionInterpretV2
(
	input [31:0] instruction,
	
	//Note: not all of these will be valid at a given instant
	//It is expected the module/control logic knows which instruction type
	//is being used and therefore which of these is valid
	output [6:0] funct7,
	output [4:0] rs2,
	output [4:0] rs1,
	output [2:0] funct3,
	output [4:0] rd,
	output [6:0] opcode,//always valid
	
	//Immediates (already processed)
	output [31:0] immediateI,
	output [31:0] immediateS,
	output [31:0] immediateB,
	output [31:0] immediateU,
	output [31:0] immediateJ
);

assign funct7 = instruction[31:25];
assign rs2 = instruction[24:20];
assign rs1 = instruction[19:15];
assign funct3 = instruction[14:12];
assign rd = instruction[11:7];
assign opcode = instruction[6:0];

//Immediates
assign immediateI[31:12] = {20{instruction[31]}};//sign extended
assign immediateI[11:0] = instruction[31:20];

assign immediateS[31:12] = {20{instruction[31]}};//sign extended
assign immediateS[11:0] = {instruction[31:25], instruction[11:7]};

assign immediateB[31:13] = {19{instruction[31]}};//sign extended
assign immediateB[12:1] = {instruction[31], instruction[7], instruction[30:25], instruction[11:8]};
assign immediateB[0] = 1'b0;//Multiples of 2 only

assign immediateU[31:12] = instruction[31:12];
assign immediateU[11:0] = 12'b000000000000;//both LUI and AUIPC zero out the bottom 12 bits, so we do that here

assign immediateJ[31:21] = {11{instruction[31]}};//sign extended
assign immediateJ[20:1] = {instruction[31], instruction[19:12], instruction[20], instruction[30:21]};
assign immediateJ[0] = 1'b0;//Multiples of 2 only

endmodule
