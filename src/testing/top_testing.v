//Note: This is not part of the core, just a file for me to use
//to test the core on my devboard
//To actually use the core, just jcorea.v and its dependencies are needed (everything in the jcorea folder)

module top_testing
(
	input clock,
	input not_reset,
	
	//testing
	//7 segment display
	output [7:0] segment,
	output [3:0] digit,
	
	//4 buttons
	input [3:0] not_button,
	
	output [3:0] not_led,
	
	output [7:0] logicAnalyzerOut
);
wire reset = ~not_reset;
reg clock25 = 1'b0;

always @(posedge clock)
begin
	clock25 = ~clock25;
end

//First working c program :) memFiles/memorymappediowritetest.mem
//jcorea core (.clock(clock25), .reset(reset));
//jcorea #(.INITIAL_MEM_CONTENTS("memFiles/memorymappediowritetest.mem")) (.clock(clock25), .reset(reset), .register31Output(testView), .busView(busView));

/* Testing Section */
//assign logicAnalyzerOut = 8'b01010101;
wire [15:0] displayOutput;
wire [3:0] button = ~not_button;
wire [3:0] led;
assign not_led = ~led;

wire [31:0] testView;
wire [31:0] busView;

//wire coreClockInput = displayprescaler[25];
wire coreClockInput = displayprescaler[18];
assign led[0] = coreClockInput;
assign logicAnalyzerOut[7] = coreClockInput;

//Whole core testing (broken)
//assign displayOutput = busView;
assign displayOutput = testView;
//assign led[3] = testView[0];
//assign logicAnalyzerOut[6:0] = busView;
assign logicAnalyzerOut[6:0] = testView;

//Testing port io
wire [31:0] portATestDir;
wire [31:0] portATest;
//Only put data onto port when data direction is reading
assign portATest[0] = (portATestDir[0] == 0) ? button[0] : 1'bz;
assign portATest[1] = (portATestDir[1] == 0) ? button[1] : 1'bz;
assign portATest[2] = (portATestDir[2] == 0) ? button[2] : 1'bz;
assign portATest[3] = (portATestDir[3] == 0) ? button[3] : 1'bz;

assign led[3:1] = portATest[7:5];

//First working c program :) memFiles/memorymappediowritetest.mem
//jcorea #(.INITIAL_MEM_CONTENTS("memFiles/memorymappediowritetest.mem")) (.clock(coreClockInput), .reset(reset), .register31Output(testView), .busView(busView), .portA(portATest), .portADir(portATestDir));//using prescaler to make things slower
jcorea #(.INITIAL_MEM_CONTENTS("memFiles/memorymappediowritetest.mem")) (.clock(clock25), .reset(reset), .register31Output(testView), .busView(busView), .portA(portATest), .portADir(portATestDir));//no prescaler
			  
//Output
reg [31:0] displayprescaler;

always @(posedge clock)
begin
	displayprescaler = displayprescaler + 17'b1;
end

//multinum7seg(.displayclk(displayprescaler[17]), .number(displayOutput), .segment(segment), .digit(digit));

//multi7seg (.clock(displayprescaler[17]), .data0(displayOutput[15:12]), .data1(displayOutput[11:8]), .data2(displayOutput[7:4]), .data3(displayOutput[3:0]), .segment(segment), .ground(digit));

endmodule